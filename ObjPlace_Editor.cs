﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

[CustomEditor(typeof(GameObject))]
public class ObjPlace_Editor : Editor {
	private RaycastHit Hit;
	private GameObject ObjPos;
	public float distance= 10f;
	public float ObjectDist= 2f;

	void OnSceneGUI(){
		Handles.BeginGUI ();
		if (GUILayout.Button ("Save")) {
			SeeIfAllOk ();
		}
		Handles.EndGUI ();

	}

	void SeeIfAllOk(){

		if (Selection.activeGameObject != null) {
			ObjPos = Selection.activeGameObject;
			if (Physics.Raycast (ObjPos.transform.position, Vector3.down, out Hit, distance)) {
				if (Hit.collider != null) {
					if (Hit.collider.tag == "Plane") {
						Vector3 TargetLoc = Hit.point;

						if (ObjPos.tag != "Road") {
							TargetLoc += new Vector3 (0f, Selection.activeGameObject.transform.localScale.y / 2, 0f);
							ObjPos.transform.position = TargetLoc;
						}
					} 
					if (Hit.collider.tag == "Road") {
						string error = "Unable to save because" + ObjPos.name + "is on road";
						Debug.Log (error);

					}
				}
			}
			if (Physics.Raycast (ObjPos.transform.position, Vector3.right, out Hit, ObjectDist)) {
				if (Hit.collider != null) {
					if (Hit.collider.tag == "Building") {
						float GetPos = Hit.collider.transform.position.x;
						float XScale = Hit.collider.transform.localScale.x/2;
						float PlaceObject = GetPos - XScale;

						float GetCurrPos = ObjPos.transform.position.x;
						float CurrObj_XScale = ObjPos.transform.localScale.x/2;
						float RightBorder = GetCurrPos + CurrObj_XScale;


						//float GetDist = Mathf.Abs (PlaceObject - RightBorder);

						//float Add = PlaceObject + RightBorder;
						//if (GetDist > 0) {

					//	}
						ObjPos.transform.position = new Vector3 (PlaceObject + (PlaceObject/2) , ObjPos.transform.position.y, ObjPos.transform.position.z);
					}
				}
			}
		}
	}
}
