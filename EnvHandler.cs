﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class EnvHandler : MonoBehaviour {

	LevelData leveldata;
	private string LoadDataFilePath= "/StreamingAssets/data.json";
	// Use this for initialization
	private List <RootInfo> rootobjs;
	private List <ParentInfo> parentobjs;
	private List <ChildInfo> childobjs;
	//private List <GameObject> Prefabs;
	private object [] Prefabs;

	void Awake (){
		leveldata = new LevelData();
		rootobjs = new List <RootInfo> ();
		parentobjs = new List<ParentInfo> ();
		childobjs = new List <ChildInfo> ();

		AddPrefabs_ToList ();
		LoadObjectsData ();
		MatchPrefabsData ();
	}
	void Start () {
		//LoadInCorrectPlaces ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LoadObjectsData (){
		string FilePath = Application.dataPath + LoadDataFilePath;

		if (File.Exists (FilePath)) {
			string DataAsJson = File.ReadAllText (FilePath);
			leveldata = JsonUtility.FromJson <LevelData> (DataAsJson);
		}
	}

	void MatchPrefabsData (){
		List <GameObject> temp_prefabs = new List <GameObject> ();
		List <RootInfo> Temp_Root = new List <RootInfo> ();
		foreach (GameObject prefabs in Prefabs) {
			for (int i = 0; i < leveldata.RootObjects.Count; i++) {
				if (prefabs.name == leveldata.RootObjects [i].RootName) {
					temp_prefabs.Add (prefabs);
					Temp_Root.Add (leveldata.RootObjects [i]);
				}
			}
		}

		for (int j = 0; j < temp_prefabs.Count; j++) {
			if (temp_prefabs [j].transform.childCount > 0) {
				SetParents (temp_prefabs [j].transform);
			}
			InstantiatePrefab (temp_prefabs [j], Temp_Root [j]);
		}
	}


	void InstantiatePrefab(GameObject prefab, RootInfo objfromfile){
		GameObject inst = Instantiate (prefab,objfromfile.position, Quaternion.identity) as GameObject;
		inst.transform.localScale = objfromfile.LocalScale;
		inst.transform.eulerAngles = objfromfile.rotation;
		//SetParents (inst.transform);
	}


	void SetParents(Transform root){
		List <Transform> Parents_List = new List <Transform> ();
		List <ParentInfo> TempList = new List <ParentInfo> ();
		Transform[] t = root.GetComponentsInChildren <Transform> ();

		foreach (Transform ParentT in t) {
			if (ParentT.childCount > 0) {
				if (ParentT == t [1].gameObject.transform.root) {
				} else {
					Parents_List.Add (ParentT);
				}
			}
		}

		for (int i = 0; i < leveldata.Parents.Count; i++) {
			if (leveldata.Parents [i].RootName == root.name) {
				TempList.Add (leveldata.Parents [i]);
			}
		}

		for (int i =0; i < TempList.Count; i++) {
			if (TempList[i].ObjName == Parents_List[i].name) {
				Parents_List [i].position = leveldata.Parents [i].position;
				Parents_List [i].eulerAngles = leveldata.Parents [i].rotation;
				Parents_List [i].localScale = leveldata.Parents [i].LocalScale;
				GetAndSetChildren (Parents_List[i], root);

			}
		}
	}


	void GetAndSetChildren(Transform Parent, Transform root){
		List <ChildInfo> tempchilds = new List <ChildInfo> ();
		List <Transform> Temp_Transforms = new List <Transform> ();
		for (int i =0; i < leveldata.Children.Count; i++) {
			if (leveldata.Children [i].RootName == root.name) {
				if (leveldata.Children [i].ParentName == Parent.name) {
					tempchilds.Add (leveldata.Children [i]);
				}
			}
		}

		foreach (Transform child in Parent) {
			Temp_Transforms.Add (child);
		}

		for (int j =0; j < tempchilds.Count; j++) {
			if (Temp_Transforms [j].GetComponent<EnumScript> () != null) {
				EnumScript type = Temp_Transforms [j].GetComponent<EnumScript> ();
				string en = tempchilds [j].EnumType;
				type.type = (EnumScript.Type)System.Enum.Parse (typeof(EnumScript.Type),en);
			}

			if (Temp_Transforms [j].GetComponent <EnumScript> () == null) {
				EnumScript en_n = Temp_Transforms [j].gameObject.AddComponent (typeof(EnumScript)) as EnumScript;

				EnumScript typ = Temp_Transforms [j].GetComponent <EnumScript> ();
				if (tempchilds [j].EnumType != "") {
					typ.type = (EnumScript.Type)System.Enum.Parse (typeof(EnumScript.Type), tempchilds [j].EnumType);
				}
			}
				Temp_Transforms [j].position = tempchilds [j].position;
				Temp_Transforms [j].eulerAngles = tempchilds [j].rotation;
				Temp_Transforms [j].localScale = tempchilds [j].LocalScale;
				Debug.Log (tempchilds [j].ObjName);
		}
	}


	void AddPrefabs_ToList (){
		Prefabs = Resources.LoadAll ("Prefabs", typeof(GameObject));
	}
}
