﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class DataManager : EditorWindow  {

	//private List <GameObject> RootObjects;
	public LevelData leveldata;
	private string DataFilePath= "/StreamingAssets/data.json";
	private List <GameObject> Ins_GO;
	private RaycastHit Hit;
	public float distance= 10f;
	public float ObjectDist= 2f;
	private int Errors = 0;


	[MenuItem("Window/Level Editor")]
	static void InIt(){
		EditorWindow.GetWindow (typeof(DataManager)).Show ();
	}

	void OnGUI(){
		
		if (GUILayout.Button ("Save")) {
			InstantiateAllPrefabs ();
			CheckErrors ();
			if (Errors != 1) {
				GetAllGameObjects ();
				SaveSceneData ();
			}
			DestroyInstGO ();
		}

		if (GUILayout.Button ("Load")) {
			InstantiateAllPrefabs ();
		}

		/*if (GUILayout.Button ("Load")) {
			LoadJSONData ();
			Edit_Load = true;
			InstantiateAllPrefabs ();
		}*/
	}


	/*void LoadJSONData (){
		string FilePath = Application.dataPath + DataFilePath;

		if (File.Exists (FilePath)) {
			string DataAsJson = File.ReadAllText (FilePath);
			leveldata = JsonUtility.FromJson <LevelData> (DataAsJson);
			ObjectsList = leveldata.Objects_List;
		}
	}*/

	void GetAllGameObjects(){
		leveldata = new LevelData ();
		leveldata.RootObjects = new List <RootInfo> ();
		leveldata.Parents = new List <ParentInfo> ();
		leveldata.Children = new List <ChildInfo> ();
		for (int i =0; i < Ins_GO.Count; i++) {
			GameObject go = Ins_GO [i];
			Transform CurrTrans = go.transform;
			StoreGameObjects (go,CurrTrans);
		}
	}


	void StoreGameObjects (GameObject go, Transform CurrTrans){
		/*if (go != null) {
			if (go.GetComponent <EnumScript> () == null) {
				EnumType = null;
			} 
			if (CurrTrans.childCount > 0) {
				EnumScript[] ObjEnums = go.GetComponentsInChildren<EnumScript> ();
				foreach (EnumScript component in ObjEnums) {
					GameObject childgo = component.gameObject;
					Transform childtr = component.gameObject.transform;
					EnumType = component.type.ToString ();
					ObjectInfo Child_ObjInfo = new ObjectInfo (childtr.position, childtr.eulerAngles, childtr.localScale, EnumType, childgo.name, childtr.root.name);
					leveldata.Objects_List.Add (Child_ObjInfo);
				}
			}
		}*/
		RootInfo rootinfo = new RootInfo (CurrTrans.position, CurrTrans.eulerAngles, CurrTrans.localScale, CurrTrans.childCount, CurrTrans.name);
		leveldata.RootObjects.Add (rootinfo);
		if (CurrTrans.childCount > 0) {
			GetParentObjects (CurrTrans);
		}

	}


	void GetParentObjects (Transform CurrT){
		EnumScript en;
		string enstr= "";
		Transform[] t = CurrT.GetComponentsInChildren <Transform> ();

		foreach (Transform ParentT in t) {
			if (ParentT.childCount > 0) {
				if (ParentT == t [1].gameObject.transform.root) {
				} else {
					if (ParentT.GetComponent<EnumScript> () != null) {
						en = ParentT.GetComponent <EnumScript>();
						enstr = en.type.ToString ();
					} else {
						enstr = null;
					}
					ParentInfo parentinfo = new ParentInfo (ParentT.position, ParentT.eulerAngles, ParentT.localScale,enstr, ParentT.name, ParentT.root.name,ParentT.parent.name);
					//temp.Add (ParentT);
					//Debug.Log (ParentT.name);
					leveldata.Parents.Add (parentinfo);
					AddChildren (ParentT);
				}
			}

		}
	}


	void AddChildren(Transform ParentT){
		EnumScript en_child;
		string enumstr = "";
		foreach (Transform child in ParentT) {
			if (child.GetComponent<EnumScript> () != null) {
				en_child = child.GetComponent <EnumScript> ();
				enumstr = en_child.type.ToString ();
			} else {
				en_child = null;
			}
			ChildInfo childinfo = new ChildInfo(child.position, child.eulerAngles, child.localScale, enumstr, child.name, child.parent.name,child.root.name);
			leveldata.Children.Add (childinfo);
		}
	}

	void SaveSceneData(){
		string DataAsJson = JsonUtility.ToJson (leveldata);
		string FilePath = Application.dataPath + DataFilePath;
		File.WriteAllText (FilePath, DataAsJson);
		Debug.Log ("Saved");
	}

	void InstantiateAllPrefabs(){
		List <Transform> Temp_Child= new List <Transform>();
		List <string> Temp_Parent= new List <string>();
		Ins_GO = new List<GameObject> ();
		Object[] ListObject = Resources.LoadAll ("Prefabs", typeof(GameObject));
		foreach (GameObject listobj in ListObject) {
			GameObject inList = (GameObject)PrefabUtility.InstantiatePrefab(listobj);
			inList.transform.position = listobj.transform.position;
			inList.transform.rotation = listobj.transform.rotation;
			Ins_GO.Add (inList);
		}
		/*if (Edit_Load) {
			string EnType = "";
			for (int i = 0; i < ObjectsList.Count; i++) {
				foreach (GameObject listobj in ListObject) {
					if (ObjectsList [i].ParentName == listobj.name) {
						if (Temp_Parent.Contains (listobj.name)) {
							Debug.Log ("Duplicate Parent");
						} else {
							GameObject inList = (GameObject)EditorUtility.InstantiatePrefab (listobj);
							inList.transform.position = listobj.transform.position;
							inList.transform.rotation = listobj.transform.rotation;
							Temp_Parent.Add (inList.name);
						}
						//EnumScript[] ObjEnums = listobj.GetComponentsInChildren<EnumScript> ();
						//EnumScript [] Objs= listobj.GetComponentsInChildren<EnumScript>();
						Transform[] Objs = listobj.GetComponentsInChildren<Transform> ();
						foreach (Transform obj in Objs) {
							if (obj.GetComponent<EnumScript> () != null) {
								EnumScript en = new EnumScript ();
								en = obj.gameObject.GetComponent <EnumScript> ();
								if (ObjectsList [i].EnumType == en.type.ToString ()) {
									if (Temp_Child.Contains (obj)) {
									} else {
										obj.position = ObjectsList [i].position;
										obj.eulerAngles = ObjectsList [i].rotation;
										obj.localScale = ObjectsList [i].LocalScale;
										Temp_Child.Add (obj);
									}
								}
							}
						}

					}
				}
			}
		} 
		else {
			foreach (GameObject listobj in ListObject) {
				GameObject inList = (GameObject)EditorUtility.InstantiatePrefab (listobj);
				inList.transform.position = listobj.transform.position;
				inList.transform.rotation = listobj.transform.rotation;
				Ins_GO.Add (inList);  
			}
		}*/
	}

	void DestroyInstGO(){
		foreach (GameObject inList in Ins_GO) {
			DestroyImmediate (inList);
		}
	}


	void CheckErrors(){
		List <GameObject> Child_Objs = new List <GameObject> ();
		bool overlapping;
		for (int i =0; i < Ins_GO.Count; i++) {
			Transform go_tr = Ins_GO [i].transform;
			//GetHitPoint (go_tr);
			if (go_tr.childCount > 0) {
				Transform [] children = go_tr.GetComponentsInChildren<Transform>();
				foreach (Transform child in children) {
					if (child.GetComponent<MeshRenderer> () != null) {
						GetHitPoint (child);
						Child_Objs.Add (child.gameObject);
					}
				}
			}
		}

		for (int i = 0; i < Child_Objs.Count; i++) {
			foreach (GameObject obj in Child_Objs) {
				if (Child_Objs [i] == obj) {
					break;
				}
				Collider col1 = Child_Objs [i].GetComponent<Collider> ();
				Collider col2 = obj.GetComponent <Collider> ();
				overlapping = col1.bounds.Intersects (col2.bounds);
				if (overlapping) {
					Debug.Log (col1.name + " " + "is overlapping with" + " " + col2.name);
					Errors = 1;
				}
			}
		}
	}


	void GetHitPoint(Transform go_tr){
		if (Physics.Raycast (go_tr.position, Vector3.down, out Hit, distance)) {
			if (Hit.collider != null) {
				if (Hit.collider.tag == "Plane") {
					Vector3 TargetLoc = Hit.point;

					if (go_tr.tag != "Road") {
						TargetLoc += new Vector3 (0f, go_tr.localScale.y / 2, 0f);
						go_tr.position = TargetLoc;
					}
				} 
				if (Hit.collider.tag == "Road") {
					string error = "Unable to save because" + " " + go_tr.name + " " + "is on road";
					Debug.Log (error);
					Errors = 1;
				}
			}
		}
	}
}
