﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData {
	public List <RootInfo> RootObjects;
	public List <ParentInfo> Parents;
	public List <ChildInfo> Children;

}

[System.Serializable]
public class ParentInfo{
	public Vector3 position;
	public Vector3 rotation;
	public Vector3 LocalScale;
	//public string ObjTag;
	public string EnumType;
	public string ObjName;
	public string RootName;
	public string ParentName;

	public ParentInfo (Vector3 position, Vector3 rotation, Vector3 localscale, string EnumType, string Name,string RootName, string ParentName){
		this.position = position;
		this.rotation = rotation;
		this.LocalScale = localscale;
		//this.ObjTag = Tag;
		this.EnumType= EnumType;
		this.ObjName = Name;
		this.RootName = RootName;
		this.ParentName = ParentName;
	}
}

[System.Serializable]
public class RootInfo{
	public Vector3 position;
	public Vector3 rotation;
	public Vector3 LocalScale;
	public int ChildCount;
	public string RootName;

	public RootInfo (Vector3 position, Vector3 rotation, Vector3 localscale, int ChildCount, string RootName){
		this.position = position;
		this.rotation = rotation;
		this.LocalScale = localscale;
		this.ChildCount = ChildCount;
		this.RootName = RootName;
	}
}

[System.Serializable]
public class ChildInfo{
	public Vector3 position;
	public Vector3 rotation;
	public Vector3 LocalScale;
	public string EnumType;
	public string ObjName;
	public string ParentName;
	public string RootName;

	public ChildInfo (Vector3 position, Vector3 rotation, Vector3 localscale, string EnumType, string ObjName, string ParentName, string RootName){
		this.position = position;
		this.rotation = rotation;
		this.LocalScale = localscale;
		this.EnumType = EnumType;
		this.ObjName = ObjName;
		this.ParentName = ParentName;
		this.RootName = RootName;
	}

}

